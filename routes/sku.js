module.exports = (app) => {
    const sku = require('../controllers/sku.controller.js');

    app.post('/create-sku', sku.createSKU);
    app.get('/get-sku', sku.getAllSKU);
};
