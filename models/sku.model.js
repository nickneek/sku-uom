/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
require('dotenv').config();
const { DB_HOST, DB_USER, DB_PASSWORD, DB, DB_LIMIT, DB_PORT } = process.env;
var qp = require('@flexsolver/flexqp2');
qp.presetConnection(DB_HOST, DB_USER, DB_PASSWORD, DB, DB_LIMIT, DB_PORT);

// constructor
const SKU = function (expense) {
    this.username = expense.username;
};

SKU.createSKU = async (req, result) => {
    try {
        const infoDao = buildArrayDao(req.body[0]);
        const skuDao = buildSkuArrayDao(req.body);

        const info = await qp.run('INSERT INTO info SET ?', [infoDao[0]]);
        skuDao[0][0].info_id = info.insertId;
        const skuResult = await qp.run('INSERT INTO sku SET ?', [skuDao[0][0]]);

        for (const el of skuDao) {
            const uom = el[1].reduce((a, i) => [...a, Object.values(i)], []);

            const barcode = el[2].reduce(
                (a, i) => [...a, Object.values(i)],
                []
            );

            const hashtag = el[3].reduce(
                (a, i) => [...a, Object.values(i)],
                []
            );

            const uoms = await qp.run(
                'INSERT INTO sku_uom (sku_id, uom_id) VALUES ?',
                [uom]
            );

            const barcodes = await qp.run(
                'INSERT INTO sku_barcode (sku_id, barcode_id) VALUES ?',
                [barcode]
            );

            const hashtags = await qp.run(
                'INSERT INTO sku_hashtag (sku_id, hashtag_id) VALUES ?',
                [hashtag]
            );
        }

        result({ error: false, message: 'SKU successfully created!' });
    } catch (err) {
        console.log(err);
        result(err);
    }

    function buildArrayDao(body) {
        const infoArr = [];

        for (const e of body.info) {
            const dao = {
                inventorise: e.inventorise,
                wastage: e.wastage,
                threshold: e.threshold,
                max_order_qty: e.max_order_qty,
                remarks: e.remarks,
                supplier_desc: e.supplier_desc,
                product_brand: e.product_brand,
                packaging_detail: e.packaging_detail,
            };

            infoArr.push(dao);
        }

        return infoArr;
    }

    function buildSkuArrayDao(sku) {
        const arrDao = [];
        const uomArr = [];
        const hashtagArr = [];
        const barcodeArr = [];

        for (const e of sku) {
            for (const uom of e.uom) {
                uomArr.push({
                    sku_id: e.sku_id,
                    uom_id: uom.uom_id,
                });
            }

            for (const barcode of e.barcodes) {
                barcodeArr.push({
                    sku_id: e.sku_id,
                    barcode_id: barcode.barcode_id,
                });
            }

            for (const hashtag of e.hashtags) {
                hashtagArr.push({
                    sku_id: e.sku_id,
                    hashtag_id: hashtag.hashtag_id,
                });
            }
            const dao = [
                {
                    id: e.sku_id,
                    display_name: e.display_name,
                    display_name_ch: e.display_name_ch,
                    display_name_alt: e.display_name_alt,
                    status: e.status,
                    category_id: e.category_id,
                },
                uomArr,
                barcodeArr,
                hashtagArr,
            ];

            arrDao.push(dao);
        }

        return arrDao;
    }
};

SKU.getAllSKU = async (user, result) => {
    let sku =
        await qp.select(`SELECT s.id, s.display_name, s.display_name_ch, c.category_name, u.uom_name AS primary_uom, i.inventorise, s.status 
			FROM sku s
			INNER JOIN info i on i.id = s.info_id
			INNER JOIN category c on c.id = s.category_id
			INNER JOIN sku_uom su on su.sku_id = s.id
			INNER JOIN uom u on u.id = su.uom_id;`);
    result(sku);

    return sku;
};

module.exports = SKU;
