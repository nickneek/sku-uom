/* eslint-disable no-undef */
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');

var app = express();
app.use(cookieParser());

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));

require('./routes/sku')(app);

module.exports = app;
