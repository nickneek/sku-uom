const SKU = require('../models/sku.model.js');

// Add expenses to project
exports.createSKU = (req, res) => {
    SKU.createSKU(req, (data) => {
        if (!data) {
            res.status(500).json({
                message:
                    data.message ||
                    'Some error occurred while inserting expenses.',
            });
        } else {
            res.status(201).json(data);
        }
    });
};

// Retrieve all Project that matches user's id from the database.
exports.getAllSKU = (req, res) => {
    SKU.getAllSKU(req.user, (data) => {
        if (!data) {
            console.log(data);
            res.status(500).send({
                message:
                    data.message ||
                    'Some error occurred while retrieving projects.',
            });
        } else {
            res.status(201).json(data);
        }
    });
};